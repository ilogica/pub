<?php
require_once('Mobile_Detect.php');
$detect = new Mobile_Detect;
if( $detect->isMobile() && !$detect->isTablet() )
{
	define('ISMOBILE',true);
}
else
{
	define('ISMOBILE',false);
}

if(ISMOBILE)
{
	echo "Is mobile but not tablet";
}
else
{
	echo "Is not mobile but maybe a tablet";
}

?>