
$(document).ready(function() {
	/* Hora*/
	startTime();
	
	/* Fecha */
	var months = new Array ("ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic");
	var days = new Array("DOMINGO","LUNES","MARTES","MI&Eacute;RCOLES","JUEVES","VIERNES","S&Aacute;BADO");
	curdate = new Date();
	html = '<h2>'+curdate.getDate()+'</h2><h3>'+months[curdate.getMonth()]+'</h3>';
	$(".fecha").html(html);
	
	/* Temperatura */
	$.simpleWeather({
		location: 'Valparaiso, CL',
		woeid: '',
		unit: 'c',
		success: function(weather) {
		html = '<span border="0" class="weather weather-'+weather.code+'"></span><h3>'+weather.temp+'º</h3>';
		$(".tiempo").html(html);
		},
		error: function(error) {
		$(".tiempo").html('<p>'+error+'</p>');
		}
	});
});

function startTime() {
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	m = addZero(m);
	h = addZero(h);
	$('.hora').html('<h3>'+h+":"+m+"</h3>");
	setTimeout(function(){startTime()},500);
}

function addZero(i) {
	return i<10?"0"+i:i;
}
